from django.contrib.auth import get_user_model
from config import celery_app

from celery.utils.log import get_task_logger

User = get_user_model()
logger = get_task_logger(__name__)


@celery_app.task()
def get_users_count():
    """A pointless Celery task to demonstrate usage."""
    return User.objects.count()
