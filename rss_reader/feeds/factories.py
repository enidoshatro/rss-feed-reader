from ..users.models import User

from typing import List, Optional, Dict
from django.utils import timezone
import time
import pytz
from .entities import Feed as FeedEntity, Article as ArticleEntity
from django.utils.html import strip_tags


def feed_factory(feed_url: str, user: Optional[User], feed_data: Dict) -> FeedEntity:
    """Feed entity factory from parsed feed data.

    :param feed_url: The feed url which to fetch.
    :type feed_url: str
    :param user: The user doing the request for this feed. Not always present.
    Usually they are connect at a later state in the respective repository.
    :param feed_data: Parsed feed data.
    :return: A populated Feed entity with parsed feed data.
    :rtype: Feed
    """
    return FeedEntity(
        title=feed_data.get("title"),
        feed_url=feed_url,
        text=feed_data.get("title"),
        site_url=feed_data.get("link"),
        frequency=60,
        user=user,
        last_updated=_get_datetime(feed_data.get(
            'updated_parsed', feed_data.get(
                'published_parsed', None
            ),
        )),
        last_checked=timezone.now(),
    )


def articles_factory(feed: Optional[FeedEntity], feed_entries: List) -> List[ArticleEntity]:
    """Articles factories.

    :param feed: The feed entity this article is related to. Not always present.
    It is possible to connect Feed with this article at a later state.
    :param feed_entries: Parsed feed articles/entries to generate Articles from.
    :return: a list with Article entities.
    :rtype: List[Article]
    """
    return [
        article_factory(feed, entry) for entry in feed_entries
    ]


def article_factory(feed: Optional[FeedEntity], data: Dict) -> ArticleEntity:
    """Article Entity factory from parsed feed article data.

    :param feed: The feed entity this article is related to. Not always present.
    It is possible to connect Feed with this article at a later state.
    :param data: Parsed article data from feed.
    :return: The populated Article Entity.
    :rtype: Article
    """
    # Get the content and strip tag it.
    content = data.get('content', None)
    if not content:
        content = data.get('description', '')
    content = strip_tags(content)

    # Order: updated, published, created
    # If not provided
    # Will default to current time.
    date = data.get(
        'updated_parsed', data.get(
            'published_parsed', data.get(
                'created_parsed', None
            )
        )
    )
    date = _get_datetime(date)
    url = data.get('link', '')

    return ArticleEntity(
        feed=feed,
        title=data.get('title', ''),
        content=content,
        date=date,
        url=url,
        guid=data.get('guid', url),
        author=data.get('author', ''),
        comments_url=data.get('comments', ''),
        expires=None
    )


def _get_datetime(datetime_raw) -> timezone.datetime:
    """Helper function to parse the raw date coming from feed and articles data.

    :param datetime_raw: raw date time for parsed feed.
    :return: The parsed datetime timezone aware.
    :rtype: datetime
    """
    try:
        return timezone.datetime.fromtimestamp(
            time.mktime(datetime_raw), tz=pytz.UTC
        )
    except TypeError:
        pass
