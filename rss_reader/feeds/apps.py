from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class FeedsConfig(AppConfig):
    name = "rss_reader.feeds"
    verbose_name = _("Feeds")

    def ready(self):
        try:
            import rss_reader.feeds.signals  # noqa F401
        except ImportError:
            pass
