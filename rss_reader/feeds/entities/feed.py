from django.utils import timezone
from ..models import User
from typing import Optional


class Feed:
    """A Feed entity. Maps the fields in models/Feed."""
    def __init__(self,
                 title: str,
                 feed_url: str,
                 text: str,
                 site_url: str,
                 frequency: int,
                 user: Optional[User],
                 last_updated: timezone,
                 last_checked: timezone = timezone.now(),
                 next_check: timezone = None,
                 error: str = "",
                 is_active: bool = True,
                 count_unread: int = 0,
                 count_total: int = 0
                 ):
        self.title = title
        self.feed_url = feed_url
        self.text = text
        self.site_url = site_url
        self.frequency = frequency

        self.user = user

        self.last_updated = last_updated
        self.last_checked = last_checked
        self.next_check = next_check or last_checked + timezone.timedelta(
            minutes=frequency,
        )

        self.error = error
        self.is_active = is_active

        self.count_unread = count_unread
        self.count_total = count_total

    def __unicode__(self):
        return f"{self.__dict__}"

    def get_attributes(self):
        return self.__dict__

    def next_poll(self):
        """Check if it's due time for a check before the next poll

        :raises: Exception Not ready for update.
        :return: True if it ready for next check.
        """
        #
        now = timezone.now()
        next_poll = now + timezone.timedelta(minutes=self.frequency)
        if (
            self.next_check is not None
            and self.next_check >= next_poll
        ):
            raise Exception("Not Ready for Updated yet or no updates from feed")
        return True

    def is_ready_for_update(self, updated: timezone, force_update: bool = False) -> bool:
        """Check if feed is ready for update or not.

        :param updated: represents the old stored model value of last updated
        :type updated: timezone.datetime
        :param force_update: Defauls to False, if try, feed is becomes ready for update.
        :type force_update: bool
        :raise Exception If it is not ready for update yet.
        :return: True if feed is ready for update. Raises Exception otherwise.
        """
        # For force update no further checks.
        if force_update:
            return True

        # self.last_update represents the newly parsed last_update
        # updated represents the old stored model value of last updated
        if not updated or updated <= self.last_updated:
            return True

        raise Exception("Not Ready for Updated yet or no updates from feed")

    def set_updated_times(self):
        """Sets updated date times.

        :return: None
        """
        # It's update time
        self.last_checked = now = timezone.now()
        self.next_check = now + timezone.timedelta(
            minutes=self.frequency or 60,
        )
        if self.last_updated is None:
            self.last_updated = now

    def check_feed_status(self, status: int):
        """Checks feed statuses. If exceptions is raised error is recorded in Feed error field and is disabled.

        :param status: The status from the parsed feed request.
        :type status: int
        :raise Exception if status is not OK!.
        :return: True if status is 200, 302, 304, 307
        """
        try:
            return self.feed_status(status)
        except Exception as e:
            self.set_error(str(e))
            self.disable()
            raise Exception(f"{e}")

    def feed_status(self, status: int) -> bool:
        """Check for feed statuses.

        :param status: The status from the parsed feed request.
        :type status: int
        :raise Exception if status is not OK! or status is not recognized.
        :return: True if status is 200, 302, 304, 307
        """
        # Accepted status:
        #   200 OK
        #   302 Temporary redirect
        #   304 Not Modified
        #   307 Temporary redirect
        if status in (200, 302, 304, 307):
            # Check for valid feed
            if (
                not self.title
                or not self.site_url
            ):
                raise Exception("Feed parsed but with invalid contents")
            return True

        # Temporary errors:
        #   404 Not Found
        #   500 Internal Server Error
        #   502 Bad Gateway
        #   503 Service Unavailable
        #   504 Gateway Timeout
        if status in (404, 500, 502, 503, 504):
            raise Exception(f"Temporary error {status}")

        # Feed gone
        if status == 410:
            raise Exception("No feed")
        raise Exception(f"Not recognized status code {status}")

    def permanent_status_redirect(self, status: int, href_feed_url: str = None) -> Optional[str]:
        """Check for permanent status redirect. Usually called before self.check_feed_status().

        :param status: The status from the parsed feed request.
        :type status: int
        :param href_feed_url: new url to fetch the feed from.
        :type href_feed_url: str
        :return: New url or None.
        :rtype str or None.
        """
        if status != 301:
            return None

        if href_feed_url is None:
            return None

        # Avoid circular redirection
        if self.feed_url == href_feed_url:
            return None

        if status == 301:
            return href_feed_url

    def set_counters(self, unread: int, total: int):
        """Sets counters of the Feed.

        :param unread: The unread feeds.
        :type unread: int
        :param total: The total feeds.
        :type total: int
        :return: None
        """
        self.count_unread = unread
        self.count_total = total

    def set_error(self, error: str):
        """Sets an error message for the feed entity. Value gets stored at the FeedModel created by this entity.

        :param error: The error message.
        :type error: str
        :return: None
        """
        self.error = error

    def disable(self):
        """Disabled the feed. Value Gets stored  later in FeedModel

        :return: None
        """
        self.is_active = False
