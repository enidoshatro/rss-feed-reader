from .feed import Feed

from django.utils import timezone
from ..constants import ARTICLE_UNREAD
from typing import Optional


class Article:
    """Article entity. Maps the fields in models/Article."""

    def __init__(self,
                 feed: Optional[Feed],
                 expires: None,
                 title: str,
                 content: str,
                 date: timezone,
                 state: int = ARTICLE_UNREAD,
                 bookmark: bool = False,
                 author: str = "",
                 url: str = "",
                 comments_url: str = "",
                 guid: str = ""
                 ):
        self.feed = feed
        self.state = state
        self.bookmark = bookmark
        self.expires = expires
        self.title = title
        self.content = content
        self.date = date
        self.author = author
        self.url = url
        self.comments_url = comments_url
        self.guid = guid

    def __unicode__(self):
        return f"{self.__dict__}"

    def get_attributes(self):
        return self.__dict__
