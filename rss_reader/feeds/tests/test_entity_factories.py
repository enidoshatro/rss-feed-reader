from ..entities import Feed as FeedEntity, Article as ArticleEntity
from ..factories import articles_factory, feed_factory, article_factory, _get_datetime
from factory import Faker
from django.utils import timezone


def generate_feed_data():
    time_tuple = (2020, 2, 17, 17, 3, 38, 1, 48, 0)
    return {
        "title": Faker("text"),
        "feed_url": Faker("url"),
        "link": Faker("url"),
        "updated_parsed": time_tuple,
    }


def generate_article_data():
    time_tuple = (2020, 2, 17, 17, 3, 38, 1, 48, 0)
    return {
        "description": "<h1>test</h1>",
        "updated_parsed": time_tuple,
        "link": Faker("url"),
        "title": Faker("text"),
        "guid": Faker("url"),
        "author": Faker("name"),
        "comments": Faker("url"),
    }


def test_feed_factory():
    feed_data = generate_feed_data()
    feed_entity = feed_factory(
        feed_url=feed_data["feed_url"],
        user=None,
        feed_data=feed_data
    )

    assert isinstance(feed_entity, FeedEntity)
    assert feed_data["title"] == feed_entity.title
    assert feed_data["title"] == feed_entity.text
    assert feed_data["link"] == feed_entity.site_url
    assert isinstance(feed_entity.last_updated, timezone.datetime)
    assert isinstance(feed_entity.last_checked, timezone.datetime)
    assert feed_entity.frequency == 60


def test_article_factory():
    article_data = generate_article_data()
    article_entity = article_factory(
        feed=None,
        data=article_data
    )

    assert isinstance(article_entity, ArticleEntity)
    assert "test" == article_entity.content
    assert article_data["title"] == article_entity.title
    assert article_data["link"] == article_entity.url
    assert article_data["guid"] == article_entity.guid
    assert isinstance(article_entity.date, timezone.datetime)


def test_articles_factory():
    article_entries = [
        generate_article_data() for _ in range(1, 15)
    ]
    articles_list = articles_factory(
        feed=None,
        feed_entries=article_entries
    )

    assert isinstance(articles_list, list)
    for item in articles_list:
        assert isinstance(item, ArticleEntity)


def test__get_datetime():
    time_tuple = (2020, 2, 17, 17, 3, 38, 1, 48, 0)
    datetime = _get_datetime(time_tuple)

    assert isinstance(datetime, timezone.datetime)

    datetime = _get_datetime(None)
    assert datetime is None

    datetime = _get_datetime("test")
    assert datetime is None

    datetime = _get_datetime(12344)
    assert datetime is None
