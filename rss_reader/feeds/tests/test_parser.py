import pytest
from unittest import mock
from django.utils import timezone
from .test_entity_factories import generate_feed_data, generate_article_data
from ..parser import parse_feed, get_feed_results
from factory import Faker
from ..factories import feed_factory, articles_factory
from ..parser import FeedResults


def test_parse_feed():
    fake_url = Faker("url")
    with pytest.raises(Exception) as error:
        parse_feed(
            feed_url=fake_url,
            user_id=4,
            feed_id=4,
            force_update=True
        )
    assert error.value.args == (f"No Articles", )


@pytest.mark.django_db
@mock.patch(
    "rss_reader.feeds.utils.feed_last_updated_date"
)
@mock.patch(
    "rss_reader.feeds.factories.articles_factory"
)
@mock.patch(
    "rss_reader.feeds.factories.feed_factory"
)
@mock.patch(
    "rss_reader.feeds.parser.get_feed_results"
)
def test_parse_feed_update_or_create(
    mock_get_feed_results,
    mock_feed_factory,
    mock_articles_factory,
    mock_feed_last_updated_date
):
    feed_data = generate_feed_data()
    feed_entries = [
        generate_article_data() for _ in range(1, 20)
    ]
    user_id = 3
    feed_id = 5
    feed_url = "http://some_url"
    mock_get_feed_results.return_value = FeedResults(200, feed_data, feed_entries, None)

    feed_mocked = feed_factory(
            feed_url=feed_url,
            user=None,
            feed_data=feed_data
    )
    feed_mocked.set_counters(19, 19)
    feed_mocked.set_counters(19, 19)

    mock_feed_factory.check_feed_status.return_value = False
    mock_feed_factory.is_ready_for_update.return_value = True

    mock_feed_factory.return_value = feed_mocked
    mock_feed_last_updated_date.return_value = timezone.now()
    articles_mocked = articles_factory(
            feed=None,
            feed_entries=feed_entries
    )
    mock_articles_factory.return_value = articles_mocked

    return_value = {
        "user_id": user_id,
        "feed": feed_mocked.get_attributes(),
        "articles": [article_mock.get_attributes() for article_mock in articles_mocked],
        "feed_id": feed_id
    }
    result = parse_feed(
        feed_url=feed_url,
        user_id=user_id,
        feed_id=feed_id,
        force_update=False
    )
    assert isinstance(result, dict)
    assert result["feed"]["title"] == return_value["feed"]["title"]
    assert result["feed"]["feed_url"] == return_value["feed"]["feed_url"]
    assert result["feed"]["count_total"] == return_value["feed"]["count_total"]
    assert result["feed"]["error"] == return_value["feed"]["error"]
    assert result["feed"]["last_updated"] == return_value["feed"]["last_updated"]

    assert isinstance(result["articles"], list)

    for key in range(len(result["articles"])):
        print(result["articles"][key])
        assert isinstance(result["articles"][key], dict)
        result["articles"][key]["title"] = return_value["articles"][key]["title"]
        result["articles"][key]["feed"] = return_value["articles"][key]["feed"]
        result["articles"][key]["date"] = return_value["articles"][key]["date"]
        result["articles"][key]["url"] = return_value["articles"][key]["url"]
        result["articles"][key]["guid"] = return_value["articles"][key]["guid"]

    assert result["feed_id"] == return_value["feed_id"]
    assert result["user_id"] == return_value["user_id"]


@pytest.mark.django_db
@mock.patch(
    "rss_reader.feeds.utils.feed_last_updated_date"
)
@mock.patch(
    "rss_reader.feeds.factories.articles_factory"
)
@mock.patch(
    "rss_reader.feeds.factories.feed_factory"
)
@mock.patch(
    "rss_reader.feeds.parser.get_feed_results"
)
def test_parse_feed_not_ready_for_update(
    mock_get_feed_results,
    mock_feed_factory,
    mock_articles_factory,
    mock_feed_last_updated_date
):
    feed_data = generate_feed_data()
    feed_data["updated_parsed"] = (2019, 2, 17, 17, 3, 38, 1, 48, 0)

    feed_entries = [
        # generate_article_data() for _ in range(1, 20)
    ]
    user_id = 3
    feed_id = 5
    feed_url = "http://some_url"
    mock_get_feed_results.return_value = FeedResults(200, feed_data, feed_entries, None)

    feed_mocked = feed_factory(
        feed_url=feed_url,
        user=None,
        feed_data=feed_data
    )

    mock_feed_last_updated_date.return_value = timezone.now()
    feed_mocked.last_updated = timezone.now() + timezone.timedelta(minutes=20)
    mock_feed_factory.return_value = feed_mocked

    articles_mocked = articles_factory(
        feed=None,
        feed_entries=feed_entries
    )
    mock_articles_factory.return_value = articles_mocked

    mock_feed = mock.MagicMock()
    mock_feed.parse.side_effect = Exception(f"Not Ready for Updated yet or no updates from feed",)
    assert mock_feed.parse_feed.called_with(
            feed_url=feed_url,
            user_id=user_id,
            feed_id=feed_id,
            force_update=False
        )
    # with pytest.raises(Exception) as error:

    # assert error.value.args == (f"Not Ready for Updated yet or no updates from feed",)


def test_get_feed_result_no_articles():
    with pytest.raises(Exception):
        get_feed_results("http://url")
