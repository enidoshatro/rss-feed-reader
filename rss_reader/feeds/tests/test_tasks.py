import pytest
from celery.result import EagerResult
from unittest import mock
from ..factories import feed_factory, articles_factory
from .test_entity_factories import generate_feed_data, generate_article_data
from rss_reader.feeds.tasks import parse_feed, store_feed, parse_and_store_feed


@pytest.mark.django_db
@mock.patch(
    "rss_reader.feeds.parser.parse_feed"
)
def test_parse_feed(mock_parse_feed_parser, settings):
    settings.CELERY_TASK_ALWAYS_EAGER = True
    feed_data = generate_feed_data()
    feed_entries = [
        generate_article_data() for _ in range(1, 20)
    ]
    user_id = 3
    feed_id = 5
    feed_url = "http://some_url"

    return_value = {
        "user_id": user_id,
        "feed": feed_factory(
            feed_url=feed_url,
            user=None,
            feed_data=feed_data
        ),
        "articles": articles_factory(
            feed=None,
            feed_entries=feed_entries
        ),
        "feed_id": feed_id
    }

    feed_parsed = mock.MagicMock()
    feed_parsed["status"] = 200
    feed_parsed["feed"] = feed_data
    feed_parsed["entries"] = feed_entries

    mock_parse_feed_parser.return_value = return_value

    # with pytest.raises(Exception) as error:

    task_result = parse_feed.delay(
        feed_url=feed_url,
        user_id=user_id,
        feed_id=feed_id,
        force_update=True
    )

    assert isinstance(task_result, EagerResult)


@pytest.mark.django_db
@mock.patch(
    "rss_reader.feeds.repositories.feed.FeedRepository.store_feed_articles_and_schedule_task"
)
def test_store_feed(mock_feed_repo_store_feed_articles_and_schedule_task, settings):
    settings.CELERY_TASK_ALWAYS_EAGER = True
    mock_feed_repo_store_feed_articles_and_schedule_task.return_value = True
    feed_result = {
        "user_id": 1,
        "feed": {},
        "articles": [],
        "feed_id": 4
    }
    task_result = store_feed.delay(feed_result)

    assert isinstance(task_result, EagerResult)
    assert task_result.result is True


@pytest.mark.django_db
@mock.patch(
    "rss_reader.feeds.repositories.feed.FeedRepository.store_feed_articles_and_schedule_task"
)
def test_store_feed_result_none(mock_feed_repo_store_feed_articles_and_schedule_task, settings):
    settings.CELERY_TASK_ALWAYS_EAGER = True
    mock_feed_repo_store_feed_articles_and_schedule_task.return_value = False
    feed_result = {
        "user_id": None,
        "feed": None,
        "articles": None,
        "feed_id": None
    }
    task_result = store_feed.delay(feed_result)

    assert isinstance(task_result, EagerResult)
    assert task_result.result is False


@pytest.mark.django_db
@mock.patch(
    "rss_reader.feeds.tasks.store_feed"
)
@mock.patch(
    "rss_reader.feeds.tasks.parse_feed"
)
def test_parse_and_store_feed(mock_parse_feed, mock_store_feed, settings):
    settings.CELERY_TASK_ALWAYS_EAGER = True

    mock_parse_feed.return_value = {
        "item": "item"
    }
    mock_store_feed.return_value = True
    task_result = parse_and_store_feed.delay(
        feed_url="some_url", user_id=3
    )

    assert isinstance(task_result, EagerResult)
    assert task_result.result is None


@pytest.mark.django_db
@mock.patch(
    "rss_reader.feeds.tasks.store_feed"
)
@mock.patch(
    "rss_reader.feeds.tasks.parse_feed"
)
def test_parse_and_store_feed_exception(mock_parse_feed, mock_store_feed, settings):
    settings.CELERY_TASK_ALWAYS_EAGER = True

    mock_parse_feed.side_effect = Exception
    mock_store_feed.side_effect = Exception
    task_result = parse_and_store_feed.delay(
        feed_url="some_url", user_id=3
    )
    assert isinstance(task_result, EagerResult)
    assert task_result.result is None
