from unittest import mock

from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils import timezone

from .factories import FeedFactory, ArticleFactory
from .test_entity_factories import generate_feed_data, generate_article_data
from factory import Faker
from ..factories import feed_factory, articles_factory
from ..repositories import FeedRepository
from ..utils import ParsedFeed
import pytest
from rss_reader.users.tests.factories import UserFactory

User = get_user_model()
pytestmark = pytest.mark.django_db


class TestFeedRepository:
    def setup(self):
        feed_data = generate_feed_data()
        feed_entries = [
            generate_article_data() for _ in range(1, 20)
        ]
        feed_url = Faker("url")

        feed = feed_factory(
            feed_url=feed_url,
            user=None,
            feed_data=feed_data
        )

        articles = articles_factory(
            feed=None,
            feed_entries=feed_entries
        )

        articles = [
            article.get_attributes() for article in articles
        ]
        self.user_id = 5
        self.feed_id = 5
        self.feed_results_data = {
            "user_id": self.user_id,
            "feed": feed.get_attributes(),
            "articles": articles,
            "feed_id": self.feed_id
        }
        self.feed_results = ParsedFeed(
            **self.feed_results_data
        )
        self.feed = None

        self.user = UserFactory()
        self.feed = FeedFactory(user=self.user)
        self.feed_repository = FeedRepository(
            feed_results=self.feed_results, feed=self.feed
        )

    def test_store_feed_articles_and_schedule_task_no_user_found(self):
        with pytest.raises(Exception) as error:
            self.feed_repository.store_feed_articles_and_schedule_task()
        assert error.value.args == ("User does not exist!",)

    @pytest.mark.django_db
    @mock.patch(
        "rss_reader.feeds.repositories.feed.FeedRepository.update_or_create_feed"
    )
    def test_store_feed_articles_and_schedule_task_feed_created_or_updated(self, mock_update_or_create_feed):
        # it means feed got updated
        mock_update_or_create_feed.return_value = False
        assert self.feed_repository.store_feed_articles_and_schedule_task() is True

        # it means feed got created
        mock_update_or_create_feed.return_value = True
        assert self.feed_repository.store_feed_articles_and_schedule_task() is True

    @pytest.mark.django_db
    @mock.patch(
        "rss_reader.users.models.User"
    )
    def test_get_user(self, mock_get_user_by_id):
        # it means feed got updated
        user = UserFactory
        mock_get_user_by_id.objects.get.return_value = user
        result = self.feed_repository.get_feed_user(user.id)
        assert isinstance(result, User)

    @pytest.mark.django_db
    def test_get_user_no_user(self):
        with pytest.raises(Exception) as error:
            self.feed_repository.get_feed_user(3)
        assert error.value.args == ("User does not exist!",)

    @pytest.mark.django_db
    @mock.patch(
        "rss_reader.feeds.repositories.feed.FeedRepository.store_articles"
    )
    @mock.patch(
        "rss_reader.feeds.repositories.feed.FeedRepository.update_or_create_feed"
    )
    def test_store_feed_articles_and_schedule_task_feed_articles_not_created(self,
                                                                             mock_update_or_create_feed,
                                                                             mock_store_articles):
        # it means feed got updated
        mock_update_or_create_feed.return_value = False
        mock_store_articles.return_value = False
        assert self.feed_repository.store_feed_articles_and_schedule_task() is False

        # it means feed got created
        mock_update_or_create_feed.return_value = False
        mock_store_articles.return_value = True
        assert self.feed_repository.store_feed_articles_and_schedule_task() is True

    @pytest.mark.django_db
    @mock.patch(
        "rss_reader.feeds.repositories.feed.FeedRepository.get_or_create_user_feed_scheduled_task"
    )
    @mock.patch(
        "rss_reader.feeds.repositories.feed.FeedRepository.update_or_create_feed"
    )
    def test_store_feed_articles_and_schedule_task_articles_not_created(self, mock_update_or_create_feed,
                                                                        mock_get_or_create_user_feed_scheduled_task):
        # it means feed got updated
        mock_update_or_create_feed.return_value = False
        mock_get_or_create_user_feed_scheduled_task.return_value = False
        assert self.feed_repository.store_feed_articles_and_schedule_task() is False

        # it means feed got created
        mock_update_or_create_feed.return_value = True
        mock_get_or_create_user_feed_scheduled_task.return_value = False
        assert self.feed_repository.store_feed_articles_and_schedule_task() is False

    def test_store_feed_articles_and_schedule_task_no_results(self):
        feed_repo = FeedRepository(
            feed_results=None, feed=None
        )
        assert feed_repo.store_feed_articles_and_schedule_task() is False

    @pytest.mark.django_db
    @mock.patch(
        "rss_reader.feeds.repositories.feed.FeedRepository.get_feed_user"
    )
    def test_update_or_create_feed(self, mock_get_user, user: settings.AUTH_USER_MODEL):
        mock_get_user.return_value = user
        assert self.feed_repository.update_or_create_feed() is True

    # @pytest.mark.django_db
    # @mock.patch(
    #     "rss_reader.feeds.models.Feed"
    # )
    # @mock.patch(
    #     "rss_reader.feeds.repositories.feed.FeedRepository.get_feed_user"
    # )
    # def test_update_or_create_feed_created_only(self, mock_get_user, mock_feed,user: settings.AUTH_USER_MODEL):
    #
    #     mock_get_user.return_value = user
    #
    #     feed_data = generate_feed_data()
    #     feed_entity = feed_factory(
    #         feed_url=feed_data["feed_url"],
    #         user=user,
    #         feed_data=feed_data
    #     )
    #
    #     feed = Feed(**feed_entity.get_attributes())
    #     feed_results_data = {
    #         "user_id": feed.user.pk,
    #         "feed": feed_entity.get_attributes(),
    #         "articles": None,
    #         "feed_id": feed.pk
    #     }
    #
    #     mock_feed.object.get.return_value = feed
    #
    #     repo = FeedRepository(feed=feed, feed_results=ParsedFeed(**feed_results_data))
    #     assert repo.update_or_create_feed() is True

    @pytest.mark.django_db
    def test_update_article(self, user: settings.AUTH_USER_MODEL):
        feed = FeedFactory(user=user)
        article = ArticleFactory(feed=feed)
        article.date = timezone.now()
        assert self.feed_repository.update_article(article) is None

        article.guid = None
        assert self.feed_repository.update_article(article) is None

        article.url = None
        assert self.feed_repository.update_article(article) is None

        article.title = None
        article.date = None
        with pytest.raises(Exception) as error:
            self.feed_repository.update_article(article)
        assert error.value.args == ('No guid, link, and title or date; cannot import', )
