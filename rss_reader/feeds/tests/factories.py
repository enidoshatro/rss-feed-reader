from ..models import Feed, Article

from factory import DjangoModelFactory, Faker
from random import randint


class FeedFactory(DjangoModelFactory):
    class Meta:
        model = Feed
        django_get_or_create = [
            "title",
            "feed_url",
            "id",
            "frequency",
            "user",
            "text",
            "last_updated",
            "created",
            "is_active",
            "next_check",
            "last_checked",
            "error",
            "count_unread",
            "count_total",
        ]

    def __init__(self, user, **kwargs):
        self.user = user

    title = Faker("text")
    feed_url = Faker("url")
    text = Faker("text")
    pk = id = randint(1, 50)
    frequency = randint(1, 50)
    last_updated = Faker("date_time")
    created = Faker("date_time")
    is_active = True
    next_check = Faker("date_time")
    last_checked = Faker("date_time")
    error = Faker("text")
    count_unread = randint(0, 10)
    count_total = randint(0, 10)


class ArticleFactory(DjangoModelFactory):

    def __init__(self, feed):
        self.feed = feed

    title = Faker("text")
    content = Faker("text")
    guid = Faker("url")
    pk = id = randint(1, 50)
    comments_url = Faker("url")
    url = Faker("url")
    author = Faker("name")
    date = Faker("date_time")
    bookmark = False
    state = randint(0, 1)
    expires = None

    class Meta:
        model = Article
        django_get_or_create = [
            "title",
            "guid",
            "feed",
            "id",
            "pk",
            "content",
            "url",
            "comments_url",
            "author",
            "date",
            "bookmark",
            "state",
            "expires"
        ]
