from ..entities import Feed, Article
from factory import Faker
from django.utils import timezone
from random import randint
import pytest


class TestFeedEntity:
    title = Faker("text")
    feed_url = Faker("url")
    site_url = Faker("url")
    frequency = randint(1, 60)
    user = None
    last_updated = timezone.now()
    last_checked = timezone.now()
    next_check = timezone.now() + timezone.timedelta(minutes=frequency)
    error = Faker("text")
    is_active = True
    count_unread = 23
    count_total = 30

    def setup(self):
        self.feed = Feed(
            title=self.title,
            feed_url=self.feed_url,
            text=self.title,
            site_url=self.site_url,
            frequency=self.frequency,
            user=self.user,
            last_updated=self.last_updated,
            last_checked=self.last_checked,
            next_check=self.next_check,
            error=self.error,
            is_active=self.is_active,
            count_unread=self.count_unread,
            count_total=self.count_total
        )

    def test_case_initialisation(self):
        assert self.feed.title == self.title
        assert self.feed.feed_url == self.feed_url
        assert self.feed.text == self.title
        assert self.feed.site_url == self.site_url
        assert self.feed.frequency == self.frequency
        assert self.feed.user == self.user
        assert self.feed.last_updated == self.last_updated
        assert self.feed.last_updated == self.last_updated
        assert self.feed.next_check == self.next_check
        assert self.feed.error == self.error
        assert self.feed.is_active == self.is_active
        assert self.feed.count_unread == self.count_unread
        assert self.feed.count_total == self.count_total

    def test_is_ready_for_update(self):
        with pytest.raises(Exception) as error:
            self.feed.is_ready_for_update(
                updated=timezone.now(), force_update=False
            )
        assert error.value.args == (f"Not Ready for Updated yet or no updates from feed",)

        assert (timezone.now() <= self.feed.last_updated) is False

        with pytest.raises(Exception) as error:
            self.feed.is_ready_for_update(
                updated=timezone.now(), force_update=False
            )
        assert error.value.args == (f"Not Ready for Updated yet or no updates from feed",)

        is_feed_ready_for_update = self.feed.is_ready_for_update(
            updated=None, force_update=True
        )
        assert is_feed_ready_for_update is True

        is_feed_ready_for_update = self.feed.is_ready_for_update(
            updated=timezone.now(), force_update=True
        )
        assert is_feed_ready_for_update is True

        self.feed.next_check = None
        is_feed_ready_for_update = self.feed.is_ready_for_update(
            updated=timezone.now() - timezone.timedelta(minutes=30), force_update=False
        )
        assert is_feed_ready_for_update is True

    def test_next_poll(self):
        assert self.feed.next_poll() is True

        self.feed.next_check = timezone.now() + timezone.timedelta(minutes=70)
        with pytest.raises(Exception) as error:
            self.feed.next_poll()
        assert error.value.args == ('Not Ready for Updated yet or no updates from feed', )

    def test_feed_status(self):
        with pytest.raises(Exception) as error:
            self.feed.feed_status(322)
        assert error.value.args == ("Not recognized status code 322",)

        with pytest.raises(Exception) as error:
            self.feed.feed_status(410)
        assert error.value.args == ("No feed",)

        with pytest.raises(Exception) as error:
            self.feed.feed_status(500)
        assert error.value.args == ("Temporary error 500",)

        with pytest.raises(Exception) as error:
            self.feed.title = None
            self.feed.site_url = None
            self.feed.feed_status(200)
        assert error.value.args == ("Feed parsed but with invalid contents",)

        self.feed.title = self.title
        self.feed.site_url = self.site_url

        assert self.feed.feed_status(200) is True

    def test_permanent_status_redirect(self):
        assert self.feed.permanent_status_redirect(200, Faker("url")) is None
        assert self.feed.permanent_status_redirect(301, None) is None
        assert self.feed.permanent_status_redirect(301, self.feed.feed_url) is None

        fake_url = Faker("url")
        assert self.feed.permanent_status_redirect(301, fake_url) == fake_url

    def test_check_feed_status(self):
        with pytest.raises(Exception) as error:
            self.feed.check_feed_status(400)
        assert error.value.args == ("Not recognized status code 400",)

    def test_get_attributes(self):
        self.feed.set_updated_times()
        assert self.feed.get_attributes() == self.feed.__dict__
        assert self.feed.__unicode__() == f"{self.feed.__dict__}"


class TestArticleEntity:
    title = Faker("text")
    content = Faker("text")
    url = Faker("url")
    author = Faker("name")
    last_updated = timezone.now()
    state = randint(0, 1)

    def setup(self):
        self.article = Article(
            feed=None,
            expires=None,
            title=self.title,
            content=self.content,
            date=self.last_updated,
            state=self.state,
            author=self.author,
            url=self.url,
        )

    def test_case_initialisation(self):
        assert self.article.feed is None
        assert self.article.title == self.title
        assert self.article.content == self.content
        assert self.article.date == self.last_updated
        assert self.article.state == self.state
        assert self.article.author == self.author
        assert self.article.url == self.url

    def test_get_attributes(self):
        assert self.article.get_attributes() == self.article.__dict__
        assert self.article.__unicode__() == f"{self.article.__dict__}"
