import pytest
from django.conf import settings
from django.test import RequestFactory
import rss_reader.feeds.views as views

pytestmark = pytest.mark.django_db


class TestFeedViews:
    def test_feeds_list(
        self, user: settings.AUTH_USER_MODEL, request_factory: RequestFactory
    ):
        view = views.FeedListView()
        request = request_factory.get("/fake-url/")
        request.user = user

        view.request = request

        assert view.template_name == "feeds/feed_list.html"
        assert view.get_object() == user

    def test_delete_view(
        self, user: settings.AUTH_USER_MODEL, request_factory: RequestFactory
    ):
        view = views.FeedDeleteView()
        request = request_factory.get("/fake-url/", kwargs={"pk": 4})
        request.user = user
        view.request = request
        assert view.success_url == "/feeds/"

    def test_feed_refresh_view(
        self, user: settings.AUTH_USER_MODEL, request_factory: RequestFactory
    ):
        view = views.FeedRefreshView()
        request = request_factory.get("/fake-url", kwargs={"pk": 4})
        request.user = user
        view.request = request

        assert view.get_redirect_url() == "/feeds/"


class TestArticlesView:
    def test_article_bookmark_redirect_view(
        self, user: settings.AUTH_USER_MODEL, request_factory: RequestFactory
    ):
        view = views.ArticleBookmarkRedirectView()
        request = request_factory.get("/fake-url", kwargs={"pk": 4})
        request.user = user

        view.request = request

    def test_article_list_view(
        self, user: settings.AUTH_USER_MODEL, request_factory: RequestFactory
    ):
        view = views.ArticlesListView()
        request = request_factory.get("/fake-url", kwargs={"pk": 4})
        request.user = user
        view.request = request
        assert view.template_name == "feeds/article_list.html"

    def test_article_details_view(
        self, user: settings.AUTH_USER_MODEL, request_factory: RequestFactory
    ):
        view = views.ArticleDetailView()
        request = request_factory.get("/fake-url", kwargs={
            "pk": 4
        })
        request.user = user

        view.request = request

        assert view.template_name == "feeds/article_detail.html"
