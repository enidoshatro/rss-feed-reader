import pytest
from django.utils import timezone
from ..utils import parse_update_dates, update_article_feed_relation, feed_last_updated_date
import datetime
from dateutil.parser import parse
from .factories import FeedFactory
from ..models import Feed
from rss_reader.users.tests.factories import UserFactory


@pytest.mark.django_db
def test_parse_update_dates():
    what_time_is_now = timezone.now()
    value_string = "2020-01-15T18:03:34.245939Z"

    item = {
        "some_date_str_timezone_aware": value_string,
        "some_other_date_str_timezone_aware": what_time_is_now,
        "some_other_key": "value"
    }

    updated_item = parse_update_dates('some_date_str_timezone_aware', item)
    assert isinstance(updated_item, dict)
    assert item is not updated_item
    assert item != updated_item
    assert item == {
        "some_date_str_timezone_aware": value_string,
        "some_other_date_str_timezone_aware": what_time_is_now,
        "some_other_key": "value"
    }

    assert updated_item == {
        "some_date_str_timezone_aware": parse(value_string),
        "some_other_date_str_timezone_aware": what_time_is_now,
        "some_other_key": "value"
    }
    assert isinstance(updated_item["some_date_str_timezone_aware"], datetime.datetime)

    another_update = parse_update_dates('non_existing_key', updated_item)
    assert isinstance(another_update, dict)
    assert updated_item is not another_update
    assert updated_item == another_update
    assert isinstance(another_update["some_date_str_timezone_aware"], datetime.datetime)

    assert another_update["some_other_date_str_timezone_aware"] == what_time_is_now
    assert another_update["some_other_key"] == "value"

    assert isinstance(updated_item["some_date_str_timezone_aware"], datetime.datetime)
    assert updated_item["some_date_str_timezone_aware"] == parse(value_string)

    and_another_one = parse_update_dates('some_other_date_str_timezone_aware', another_update)
    assert isinstance(and_another_one, dict)
    assert another_update is not and_another_one
    assert another_update == and_another_one
    assert isinstance(and_another_one["some_date_str_timezone_aware"], datetime.datetime)

    assert and_another_one["some_other_date_str_timezone_aware"] == what_time_is_now
    assert and_another_one["some_other_key"] == "value"

    assert isinstance(and_another_one["some_date_str_timezone_aware"], datetime.datetime)
    assert and_another_one["some_date_str_timezone_aware"] == parse(value_string)


def test_usage_parse_update_dates():
    what_time_is_now = timezone.now()
    value_string = "2020-01-15T18:03:34.245939Z"

    test_item = {
        "last_checked": value_string,
        "next_check": what_time_is_now,
        "last_updated": "value"
    }

    test_item = parse_update_dates(
        'last_checked',
        parse_update_dates(
            'next_check',
            parse_update_dates(
                'last_updated', parse_update_dates(
                    'some_key', test_item
                ),
            ),
        ),
    )

    assert test_item == {
        "last_checked": parse(value_string),
        "next_check": what_time_is_now,
        "last_updated": "value"
    }


@pytest.mark.django_db
def test_update_article_feed_relation():
    article = {
        "title": "test",
        "guid": "test",
        "content": "test",
        "feed": None
    }

    user = UserFactory()
    feed_model = FeedFactory(user=user)

    article_new = update_article_feed_relation(feed_model, article)

    assert article_new is not article
    assert article_new != article
    assert article_new["feed"] is not None
    assert isinstance(article_new["feed"], Feed)
    assert article_new["feed"] == feed_model

    article_1 = {
        "title": "test",
        "guid": "test",
        "content": "test",
    }
    article_new = update_article_feed_relation(feed_model, article_1)

    assert article_new is not article_1
    assert article_new != article_1
    assert article_new["feed"] is not None
    assert isinstance(article_new["feed"], Feed)
    assert article_new["feed"] == feed_model


@pytest.mark.django_db
def test_feed_last_updated_date():
    user = UserFactory()
    feed_model = FeedFactory(user=user)
    feed_date = feed_last_updated_date(feed_id=feed_model.id)
    assert isinstance(feed_model, Feed)
    assert isinstance(feed_date, datetime.datetime)

    feed_date = feed_last_updated_date(feed_id=None)
    assert feed_date is None
