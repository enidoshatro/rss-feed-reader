import pytest
from django.conf import settings
from django.urls import reverse, resolve

pytestmark = pytest.mark.django_db


def test_home_feeds():
    assert reverse("feeds:feeds") == "/feeds/"
    assert resolve("/feeds/").view_name == "feeds:feeds"


def test_add():
    assert reverse("feeds:add_feed") == "/feeds/add/"
    assert resolve("/feeds/add/").view_name == "feeds:add_feed"


def test_refresh():
    assert reverse("feeds:refresh_feed", kwargs={
        "pk": 4
    }) == "/feeds/refresh/4"
    assert resolve("/feeds/refresh/4").view_name == "feeds:refresh_feed"


def test_articles_list():
    pk = 10
    assert (
        reverse("feeds:articles", kwargs={"pk": pk})
        == f"/feeds/{pk}/"
    )
    assert resolve(f"/feeds/{pk}/").view_name == "feeds:articles"


def test_article_detail(user: settings.AUTH_USER_MODEL):
    article_pk = 10
    assert (
        reverse("feeds:article", kwargs={"pk": article_pk})
        == f"/feeds/article/{article_pk}"
    )
    assert resolve(f"/feeds/article/{article_pk}").view_name == "feeds:article"


def test_article_bookmark(user: settings.AUTH_USER_MODEL):
    article_pk = 10
    assert (
        reverse("feeds:bookmark_article", kwargs={"pk": article_pk})
        == f"/feeds/bookmark/{article_pk}"
    )
    assert resolve(f"/feeds/bookmark/{article_pk}").view_name == "feeds:bookmark_article"


def test_feed_delete(user: settings.AUTH_USER_MODEL):
    feed_pk = 10
    assert (
        reverse("feeds:delete_feed", kwargs={"pk": feed_pk})
        == f"/feeds/delete/{feed_pk}"
    )
    assert resolve(f"/feeds/delete/{feed_pk}").view_name == "feeds:delete_feed"


def test_article_delete(user: settings.AUTH_USER_MODEL):
    article_pk = 10
    assert (
        reverse("feeds:delete_article", kwargs={"pk": article_pk})
        == f"/feeds/articles/delete/{article_pk}"
    )
    assert resolve(f"/feeds/articles/delete/{article_pk}").view_name == "feeds:delete_article"
