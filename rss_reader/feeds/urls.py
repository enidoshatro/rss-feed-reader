from django.urls import path
from .views import feeds, feed_create_view, feed_delete_view, feed_refresh_view, articles, article, \
    bookmark_article_view, article_delete

app_name = "feeds"
urlpatterns = [
    path("", view=feeds, name="feeds"),
    path("add/", view=feed_create_view, name="add_feed"),
    path("refresh/<int:pk>", view=feed_refresh_view, name="refresh_feed"),
    path("<int:pk>/", view=articles, name="articles"),
    path("article/<int:pk>", view=article, name="article"),
    path("bookmark/<int:pk>", view=bookmark_article_view, name="bookmark_article"),
    path("delete/<int:pk>", view=feed_delete_view, name="delete_feed"),
    path("articles/delete/<int:pk>", view=article_delete, name="delete_article"),
]
