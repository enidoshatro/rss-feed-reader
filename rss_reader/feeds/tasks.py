from django.contrib.auth import get_user_model
from config import celery_app
from celery.utils.log import get_task_logger
from .repositories import FeedRepository
from .utils import ParsedFeed
from .parser import parse_feed as parse
from typing import Optional
User = get_user_model()
logger = get_task_logger(__name__)


@celery_app.task(name="parse_feed",
                 autoretry_for=(Exception,),
                 retry_kwargs={'max_retries': 3, 'countdown': 10},
                 retry_backoff=True)
def parse_feed(feed_url: str, user_id: int, feed_id: Optional[int] = None, force_update: bool = False):
    """A task to parse feeds.

    :param feed_url: The feed url which to fetch.
    :type feed_url: str
    :param user_id: The user id making the request to fetch a feed.
    :type user_id: int
    :param feed_id: The feed id making the request from.
    Usually it is used when force updating or from a scheduled task.
    :type feed_id: int
    :param force_update: When user wants to force update the feed. Defaults to False,
    :type force_update: bool
    :return:
    """
    return parse(feed_url, user_id, feed_id, force_update)


@celery_app.task(name="store_feed",
                 autoretry_for=(Exception,),
                 retry_kwargs={'max_retries': 3, 'countdown': 10},
                 retry_backoff=True
                 )
def store_feed(feed_results: dict):
    """A task to store the parsed feed. Used result returned only by task parse_feed.

    :param feed_results: The result to use for building feeds and articles. Result is from parsed feed data.
    :type feed_results: dict
    :raises Exception if persisting items fails.
    :return: True or false
    :rtype: bool
    """
    feed_repo = FeedRepository(
        feed_results=ParsedFeed(**feed_results), feed=None
    )
    return feed_repo.store_feed_articles_and_schedule_task()


@celery_app.task(name="parse_and_store_feed",
                 autoretry_for=(Exception,),
                 retry_kwargs={'max_retries': 3, 'countdown': 10},
                 retry_backoff=True)
def parse_and_store_feed(feed_url: str, user_id: int, feed_id: Optional[int] = None, force_update: bool = False):
    """A task to fetch and store the result of the fetch feed.

    :param feed_url: The feed url which to fetch.
    :type feed_url: str
    :param user_id: The user id making the request to fetch a feed.
    :type user_id: int
    :param feed_id: The feed id making the request from.
    Usually it is used when force updating or from a scheduled task.
    :type feed_id: int
    :param force_update: When user wants to force update the feed. Defaults to False,
    :type force_update: bool
    """
    chain = parse_feed.s(feed_url, user_id, feed_id, force_update) | store_feed.s()
    chain()
