from .models import Feed

from typing import Dict
from dateutil.parser import parse, ParserError
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from collections import namedtuple
import copy

ParsedFeed = namedtuple('ParsedFeed', 'user_id feed articles feed_id')


def parse_update_dates(key: str, item: Dict) -> Dict:
    """Helper function to parse dates from feed data.

    :param key: The key of the field in feed data.
    :param item: The actual dict holding the item.
    :return: A deep copy of the dict.
    :rtype: dict
    """
    feed_copy = copy.deepcopy(item)
    try:
        feed_copy[key] = parse(item.get(key))
    except TypeError:
        pass
    except ParserError:
        pass

    return feed_copy


def feed_last_updated_date(feed_id: int) -> timezone or None:
    """Helper function to get the feed last updated value.

    :param feed_id: the feed id to check for.
    :return: a timezone aware date or None if feed is not present.
    :rtype: timezone.datetime or None
    """
    try:
        feed = Feed.objects.get(pk=feed_id)
        return feed.last_updated
    except ObjectDoesNotExist:
        return None


def update_article_feed_relation(feed_model: Feed, article: Dict) -> Dict:
    """Helper function to update the article-feed relationship.
    Used before updating or creating an Article Model.

    :param feed_model: The FeedModel being used.
    :param article: The article dict data to insert the feed to.
    :return: A copy of the article data with the feed Model.
    :rtype: dict
    """
    article_copy = copy.deepcopy(article)
    article_copy.update(feed=feed_model)
    return article_copy
