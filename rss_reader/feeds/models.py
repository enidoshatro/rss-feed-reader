from django.db import models
from django.core.validators import URLValidator
from rss_reader.users.models import User
from .entities import Feed as FeedEntity
from django.utils import timezone
from .constants import ARTICLE_UNREAD, ARTICLE_READ


class Feed(models.Model):
    """A feed Model.

    In a many to one relationship with a User.
    The last_updated field is either the updated or published date of the feed,
    or if neither are set, defaults to timezone.now().
    """
    # Mandatory data fields
    title = models.TextField(help_text="Title of the feed")
    feed_url = models.TextField("Feed URL",
                                validators=[URLValidator()], help_text="URL of the RSS feed",
                                )
    text = models.TextField(
        "Custom title",
        blank=True,
        default=title,
        help_text="Custom title for the feed - defaults to feed title above",
    )

    # Optional data fields
    site_url = models.TextField("Site URL",
                                validators=[URLValidator()], help_text="URL of the HTML site",
                                )

    frequency = models.IntegerField(
        blank=True, null=True,
        help_text="How often to check the feed for changes, in minutes",
    )

    # Internal fields
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    created = models.DateTimeField(
        auto_now_add=True, help_text="Date this feed was created",
    )

    is_active = models.BooleanField(
        default=True,
        help_text="A feed will stop auto-updating when a permanent error occurs",
    )

    last_updated = models.DateTimeField(
        blank=True, null=True, help_text="Last time the feed says it changed",
    )
    last_checked = models.DateTimeField(
        blank=True, null=True, help_text="Last time the feed was checked",
    )
    next_check = models.DateTimeField(
        blank=True, null=True, help_text="When the next feed check is due",
    )
    error = models.CharField(
        blank=True, max_length=255, help_text="When a problem occurs",
    )

    # Cached data
    count_unread = models.IntegerField(
        default=0, help_text="Cache of number of unread items",
    )
    count_total = models.IntegerField(
        default=0, help_text="Cache of total number of items",
    )

    def __unicode__(self):
        return self.text or self.title

    class Meta:
        ordering = ('title', 'created',)

    def update(self, entity: FeedEntity):
        """Feed update.
        """
        fields = [
            'title',
            'site_url',
            'last_updated',
            'last_checked',
            'next_check',
            'error',
            'is_active',
            'count_total',
        ]
        for field in fields:
            setattr(self, field, getattr(entity, field))
        self.save()

    def save(self, *args, **kwargs):
        if self.created is None:
            self.created = timezone.now()
        super(Feed, self).save(*args, **kwargs)


class Article(models.Model):
    """Article Model.

    The actual article of the feed. in a One to Many relationship.
    """
    # Internal fields
    feed = models.ForeignKey(Feed, related_name='articles', on_delete=models.CASCADE)
    state = models.IntegerField(default=ARTICLE_UNREAD, choices=(
        (ARTICLE_UNREAD, 'Unread'),
        (ARTICLE_READ, 'Read'),
    ))
    bookmark = models.BooleanField(
        default=False,
        help_text="A bookmark field",
    )
    expires = models.DateTimeField(
        blank=True, null=True, help_text="When the entry should expire",
    )

    # Mandatory data fields
    title = models.TextField(blank=True)
    content = models.TextField(blank=True)
    date = models.DateTimeField(
        help_text="published date",
    )

    # Optional data fields
    author = models.TextField(blank=True)
    url = models.TextField(
        blank=True,
        validators=[URLValidator()],
        help_text="URL for the HTML for this entry",
    )

    comments_url = models.TextField(
        blank=True,
        validators=[URLValidator()],
        help_text="URL for HTML comment submission page",
    )
    guid = models.TextField(
        blank=True,
        help_text="GUID for the entry, according to the feed",
    )

    def __unicode__(self):
        return self.title

    def update(self, entity):
        """
        An old article has been re-published; update with new data
        """
        fields = [
            'state', 'title', 'content', 'date', 'author', 'url', 'comments_url',
            'guid',
        ]
        for field in fields:
            setattr(self, field, getattr(entity, field))
        self.save()

    def save(self, *args, **kwargs):
        if self.date is None:
            self.date = timezone.now()
        super(Article, self).save(*args, **kwargs)

    class Meta:
        ordering = ('-date',)
        verbose_name_plural = 'articles'
