from typing import Optional, Dict
import feedparser
from .factories import feed_factory, articles_factory
from .utils import feed_last_updated_date
from collections import namedtuple

FeedResults = namedtuple("FeedResults", "status feed_data entries redirect_url")


def parse_feed(feed_url: str, user_id: int, feed_id: Optional[int] = None, force_update=False) -> Optional[Dict]:
    """Main function to parse a feed with articles and related data.

    :param feed_url: The feed url which to fetch.
    :type feed_url: str
    :param user_id: The user id making the request to fetch a feed.
    :type user_id: int
    :param feed_id: The feed id making the request from.
    Usually it is used when force updating or from a scheduled task.
    :type feed_id: int
    :param force_update: When user wants to force update the feed. Defaults to False,
    :type force_update: bool
    :return: The fetched feed data in a form of a dictionary
    with the populated feed entity and article entities as dictionaries too.
    :rtype: dict
    """
    feed_results = get_feed_results(feed_url)
    feed_entity = feed_factory(feed_url=feed_url, user=None, feed_data=feed_results.feed_data)
    total = len(feed_results.entries)
    feed_entity.set_counters(unread=total, total=total)

    new_url = feed_entity.permanent_status_redirect(feed_results.status, feed_results.redirect_url)
    if new_url is not None:
        return parse_feed(new_url, user_id, feed_id, force_update)

    feed_entity.check_feed_status(feed_results.status)

    updated = feed_last_updated_date(feed_id)
    feed_entity.next_poll()
    feed_entity.is_ready_for_update(updated=updated, force_update=force_update)
    feed_entity.set_updated_times()
    article_list = articles_factory(feed=None, feed_entries=feed_results.entries)

    return {
        "user_id": user_id,
        "feed": feed_entity.get_attributes(),
        "articles": [article.get_attributes() for article in article_list],
        "feed_id": feed_id
    }


def get_feed_results(feed_url: str) -> FeedResults:
    """Standalone function to get the parsed feed data from feedparser as a named tuple.

    :param feed_url: The feed url which to fetch.
    :type feed_url: str
    :return: Parsed data.
    :raise Exception. When parsed feed has no articles.
    :rtype: FeedResults
    """
    feed_parsed = feedparser.parse(feed_url)
    status = feed_parsed.get('status', 200)
    feed_data = feed_parsed.get('feed', None)
    entries = feed_parsed.get('entries', [])
    redirect_url = feed_parsed.get('href', None)

    if len(entries) == 0:
        raise Exception('No Articles')

    return FeedResults(status, feed_data, entries, redirect_url)
