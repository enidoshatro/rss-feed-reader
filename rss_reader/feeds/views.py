from .models import Feed, Article
from .forms import AddFeedForm
from django.contrib.auth import get_user_model
from .tasks import parse_and_store_feed
from django.http import HttpResponseForbidden
from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import ObjectDoesNotExist

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DeleteView, RedirectView, DetailView
from .constants import ARTICLE_READ, ARTICLE_UNREAD
from django.utils.translation import ugettext_lazy as _
from django_celery_beat.models import PeriodicTask

User = get_user_model()


class FeedListView(LoginRequiredMixin, ListView):
    model = Feed
    template_name = "feeds/feed_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = AddFeedForm

        # TODO: Write better/raw query and load only needed list of Objects.
        user = User.objects.get(username=self.request.user.username)
        feeds_list = Feed.objects.filter(user=user)

        context['feeds'] = feeds_list

        return context

    def get_object(self):
        return User.objects.get(username=self.request.user.username)


feeds = FeedListView.as_view()


class FeedCreateView(LoginRequiredMixin, CreateView):
    model = Feed
    form_class = AddFeedForm
    template_name = "feeds/add_feed.html"

    def get_object(self, user: User):
        try:
            feed_url = self.request.POST["feed_url"]
            feed_url = feed_url.replace("http", "https")
            return Feed.objects.get(feed_url=feed_url, user=user)
        except ObjectDoesNotExist:
            return False

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()

        form = self.get_form()
        user = User.objects.get(username=request.user.username)
        self.object = self.get_object(user)

        if not form.is_valid():
            return self.form_invalid(form)

        if self.object:
            error_message = {
                "duplicate_feed_url":
                    _("This feed url has already been added.")
            }
            return HttpResponseForbidden(error_message["duplicate_feed_url"])

        # TODO: Write better/raw query and load only one Object.
        feed_url = form.cleaned_data['feed_url']
        parse_and_store_feed.delay(feed_url=feed_url, user_id=user.id)

        return self.get_success_url()

    def get_success_url(self):
        return redirect("/feeds/")


feed_create_view = FeedCreateView.as_view()


class FeedDeleteView(LoginRequiredMixin, DeleteView):
    model = Feed
    success_url = reverse_lazy("feeds:feeds")

    def get_context_data(self, *args, **kwargs):
        context = super(FeedDeleteView, self).get_context_data(**kwargs)
        # TODO: Write better/raw query and load only a list of Objects needed.
        user = get_object_or_404(User, username=self.request.user.username)
        feed = get_object_or_404(Feed, pk=self.kwargs.get('pk'), user=user)

        try:
            PeriodicTask.objects.filter(
                name=f"Automatic parse and update Feed {feed.pk} for user: {feed.user.username}",
                task="parse_and_store_feed").delete()
        except ObjectDoesNotExist:
            pass

        return context


feed_delete_view = FeedDeleteView.as_view()


class FeedRefreshView(LoginRequiredMixin, RedirectView):
    permanent = False
    query_string = True
    pattern_name = 'feeds:feeds'

    def get(self, request, *args, **kwargs):
        # TODO: Write better/raw query and load only one Object.
        feed = get_object_or_404(Feed, pk=kwargs['pk'])
        user = User.objects.get(username=request.user.username)
        parse_and_store_feed.delay(feed_url=feed.feed_url, user_id=user.id, feed_id=feed.id, force_update=True)

        return redirect("/feeds/")

    def get_redirect_url(self, *args, **kwargs):
        return super().get_redirect_url(*args, **kwargs)


feed_refresh_view = FeedRefreshView.as_view()


class ArticleBookmarkRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False
    query_string = True
    pattern_name = 'feeds:articles'

    def get(self, request, *args, **kwargs):
        # TODO: Write better/raw query and load only one Object.
        user = User.objects.get(username=self.request.user.username)
        article_detail = get_object_or_404(Article, pk=self.kwargs.get('pk'))
        feed = get_object_or_404(Feed, pk=article_detail.feed.id, user=user)

        if article_detail.bookmark is False:
            article_detail.bookmark = True
        else:
            article_detail.bookmark = False

        article_detail.save(update_fields=['bookmark'])

        return redirect(f'/feeds/{feed.id}')

    def get_redirect_url(self, *args, **kwargs):
        return super().get_redirect_url(*args, **kwargs)


bookmark_article_view = ArticleBookmarkRedirectView.as_view()


class ArticlesListView(LoginRequiredMixin, ListView):
    model = Article
    template_name = "feeds/article_list.html"
    ordering = ['-created']

    def get_context_data(self, *args, **kwargs):
        context = super(ArticlesListView, self).get_context_data(*args, **kwargs)
        # TODO: Write better/raw query and load only a list of Objects needed.
        user = User.objects.get(username=self.request.user.username)
        feed = get_object_or_404(Feed, pk=self.kwargs.get('pk'), user=user)
        articles_list = Article.objects.filter(feed=feed)
        context['articles'] = articles_list

        return context


articles = ArticlesListView.as_view()


class ArticleDetailView(LoginRequiredMixin, DetailView):
    model = Article
    template_name = "feeds/article_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # TODO: Write better/raw query and load only one Object.
        user = User.objects.get(username=self.request.user.username)
        article_detail = get_object_or_404(Article, pk=self.kwargs.get('pk'))
        feed = get_object_or_404(Feed, pk=article_detail.feed.id, user=user)

        if article_detail.state == ARTICLE_UNREAD:
            article_detail.state = ARTICLE_READ
            article_detail.save(update_fields=['state'])
            feed.count_unread = feed.count_unread - 1
            feed.save(update_fields=['count_unread'])

        context['article'] = article_detail

        return context


article = ArticleDetailView.as_view()


class ArticleDeleteView(LoginRequiredMixin, DeleteView):
    model = Article
    success_url = reverse_lazy("feeds:articles")

    def get(self, request, *args, **kwargs):
        # TODO: Write better/raw query and load only one Object.
        user = User.objects.get(username=self.request.user.username)
        article_detail = get_object_or_404(Article, pk=self.kwargs.get('pk'))
        feed = get_object_or_404(Feed, pk=article_detail.feed.id, user=user)
        if article_detail.bookmark is not True:
            article_detail.delete()
            feed.count_unread = Article.objects.filter(
                feed=feed, state=ARTICLE_UNREAD).count()
            feed.count_total = Article.objects.filter(
                feed=feed).count()
            feed.save()

        return redirect(f'/feeds/{feed.id}')

    def get_redirect_url(self, *args, **kwargs):
        return super().get_redirect_url(*args, **kwargs)


article_delete = ArticleDeleteView.as_view()
