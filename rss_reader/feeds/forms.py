from django import forms
from .models import Feed


class AddFeedForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta(forms.ModelForm):
        model = Feed
        fields = ['feed_url']
        widgets = {
            'feed_url': forms.TextInput(),
        }
