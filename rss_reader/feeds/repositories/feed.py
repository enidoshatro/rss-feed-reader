from ..models import Feed as FeedModel, Article as ArticleModel
from ..entities import Feed as FeedEntity
from ..constants import ARTICLE_UNREAD

from django.contrib.auth import get_user_model
from typing import List, Dict, Optional
from django.core.exceptions import ObjectDoesNotExist

from ..utils import parse_update_dates, update_article_feed_relation, ParsedFeed
from django_celery_beat.models import PeriodicTask, IntervalSchedule
import json

User = get_user_model()


class FeedRepository:
    """FeedRepository to create, store and schedule feed task. Create ArticleFeeds in bulk or create/update one by one.
    """
    def __init__(self, feed_results: ParsedFeed, feed: Optional[FeedModel]):
        """Constructor

        :param feed_results: The result to use for building feeds and articles. Result is from parsed feed data.
        :type feed_results: ParsedFeed
        :param feed: The feed on which to connect these newly fetched data. Might be None on init.
        :type feed: FeedModel
        """
        self.feed_results = feed_results
        self.feed = feed

    def _prepare_feed_dates(self) -> ParsedFeed:
        """Parse and return date dateutils.parsed datetime from feed data.

        :return: The complete copy of ParsedFeed data tuple with parsed and updated feed dates.
        :rtype: NamedTuple
        """
        # Feed store
        feed = parse_update_dates(
            'last_checked',
            parse_update_dates(
                'next_check',
                parse_update_dates(
                    'last_updated', self.feed_results.feed,
                ),
            ),
        )
        return ParsedFeed(
            feed_id=self.feed_results.feed_id,
            user_id=self.feed_results.user_id,
            feed=feed,
            articles=self.feed_results.articles
        )

    def store_feed_articles_and_schedule_task(self) -> bool:
        """Main method to put together all repository methods for storing the feed, storing articles
        and if everything goes ok, gets/updates or creates a new schedule task for the current user and feed.

        :return: True is successfully creates all or False. Some methods might throw Exceptions.
        :rtype: bool
        """
        if not self.feed_results or not self.feed_results.feed or not self.feed_results.articles:
            return False

        self.feed_results = self._prepare_feed_dates()
        created = self.update_or_create_feed()

        # Articles store
        are_articles_stored = self.store_articles(created)

        if not are_articles_stored:
            return False

        # get and update scheduled task or create one.
        task = self.get_or_create_user_feed_scheduled_task()

        if not task:
            return False

        return True

    def store_articles(self, created: bool):
        article_model_objects = self.prepare_articles()
        if not created and self.feed.pk:
            # update articles one by one.
            self.update_articles(article_model_objects)
            # recalculate the unread articles.
            self._update_count()
            self.feed.save()
            return True
        # create articles in bulk and return True|False.
        return self.articles_bulk_create(article_model_objects)

    def _update_count(self):
        """Simple method to update the unread and total articles
        in case of an update of articles of current feed.
        :return: None
        """
        self.feed.count_unread = ArticleModel.objects.filter(
            feed=self.feed, state=ARTICLE_UNREAD).count()
        self.feed.count_total = ArticleModel.objects.filter(
            feed=self.feed).count()

    def get_or_create_user_feed_scheduled_task(self) -> PeriodicTask:
        """Gets a task via the arguments or creates one with those args.
        Name uniqueness is guaranteed by the feed id and related user.

        :return: The newly updated or created Celery PeriodicTask.
        :rtype: PeriodicTask
        """
        schedule, created = IntervalSchedule.objects.get_or_create(
            every=self.feed.frequency,
            period=IntervalSchedule.MINUTES
        )

        kwargs = {
            "interval": schedule,
            "name": f"Automatic parse and update Feed {self.feed.pk} for user: {self.feed.user.username}",
            "task": 'parse_and_store_feed',  # name of task.

            "kwargs": json.dumps({
                "feed_url": self.feed.feed_url,
                "user_id": self.feed.user.pk,
                "feed_id": self.feed.pk,
            }),
        }

        periodic_task, created = PeriodicTask.objects.get_or_create(**kwargs)

        return periodic_task

    def get_feed_user(self, user_id: int) -> User:
        """Get user.

        :param user_id: the user id to search for.
        :type user_id: int
        :return: User.
        """
        try:
            return User.objects.get(pk=user_id)
        except ObjectDoesNotExist:
            raise Exception("User does not exist!")

    def update_or_create_feed(self) -> bool:
        """Method to create or update a feed based on ParsedFeed feed attribute data.

        :return: True if the feed is created or False if it updated.
        :rtype: bool
        """
        user = self.feed_results.feed["user"] = self.get_feed_user(self.feed_results.user_id)
        try:
            feed = FeedModel.objects.get(pk=self.feed_results.feed_id, user=user)
            feed.update(FeedEntity(**self.feed_results.feed))
            self.feed = feed

            return False
        except ObjectDoesNotExist:
            feed_created = FeedModel.objects.create(**self.feed_results.feed)
            self.feed = feed_created

            return True

    def prepare_articles(self) -> List[ArticleModel]:
        """Method to prepare ParsedFeed article data into Article Model.
        Creates an ArticleModel for each article parsed data.

        :return: A list with ArticleModel populated by article data.
        :rtype: List[ArticleModel]
        """
        def article_model(article: Dict) -> ArticleModel: return ArticleModel(**article)

        return [
            article_model(
                article=parse_update_dates(
                    key='date',
                    item=update_article_feed_relation(
                        feed_model=self.feed, article=article
                    )
                )
            )
            for article in self.feed_results.articles
        ]

    def articles_bulk_create(self, article_model_objects: List[ArticleModel]) -> bool:
        """Method to create bulk articles in case we have a new feed added.

        :param article_model_objects: A list with Article Models ready to be inserted.
        :return: True if bulk updated happened, False otherwise.
        :rtype: bool
        """
        result = ArticleModel.objects.bulk_create(
            article_model_objects
        )
        return True if result else False

    def update_articles(self, article_model_objects: List[ArticleModel]) -> None:
        [self.update_article(article) for article in article_model_objects]

    def update_article(self, article: ArticleModel):
        """Create Or Update a single Article.
        Match by feed is mandatory.
        Try to match by guid, or link, or title and date.

        :param article: ArticleModel populated by the article feed data.
        :raises Exception when query params are not there.
        :return: None
        """

        if article.guid:
            query = {
                'guid': article.guid,
            }
        elif article.url:
            query = {
                'url': article.url,
            }
        elif article.title and article.date:
            # If title and date provided, this will match
            query = {
                'title': article.title,
                'date': article.date,
            }
        else:
            raise Exception(
                "No guid, link, and title or date; cannot import"
            )
        try:
            query["feed"] = article.feed
            existing = ArticleModel.objects.get(**query)
        except ObjectDoesNotExist:
            # New article, save
            article.save()
        else:
            # Existing article
            if article.date is not None and article.date > existing.date:
                # Update Article Changes
                existing.update(article)
