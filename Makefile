.PHONY: init build run-background run-foreground logs logs-follow stop remove restart migrations migrate superuser test test-coverage open

init: build run-background migrate logs-follow

build:
	docker-compose -f local.yml build

run-foreground:
	docker-compose -f local.yml up

run-background:
	docker-compose -f local.yml up -d

logs:
	docker-compose -f local.yml logs django

stop:
	docker-compose -f local.yml stop

remove:
	docker-compose -f local.yml down

logs-follow:
	docker-compose -f local.yml logs -f django

restart:
	docker-compose -f local.yml restart

migrations:
	docker-compose -f local.yml run --rm django python manage.py makemigrations

migrate:
	docker-compose -f local.yml run --rm django python manage.py migrate

superuser:
	docker-compose -f local.yml run --rm django python manage.py createsuperuser

test:
	docker-compose -f local.yml run --rm django pytest -v

test-coverage:
	docker-compose -f local.yml run --rm django coverage run -m pytest -v && docker-compose -f local.yml run --rm django coverage html

open:
	open htmlcov/index.html
